#!/bin/bash

# Loop 200 times
for i in {1..200}; do
  # Construct the JSON payload with an increasing req_id value
  payload="{\"trigger\": \"webhook\", \"req_id\": \"request$i\"}"

  # Send the POST request using curl
  curl -X POST -H "Content-Type: application/json" -d "$payload" http://localhost:12000/trigger

  # Sleep for a short duration (adjust as needed to control request rate)
  sleep 1
done
