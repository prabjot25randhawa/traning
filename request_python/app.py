from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
import sys
import time
import random
import requests
import os
req_id=os.environ.get('req_id')

push_gateway_url = "prometheus-prometheus-pushgateway.monitoring.svc.cluster.local:9091"
registry = CollectorRegistry()

flock_url = "https://api.flock.com/hooks/sendMessage/8b1e8f06-cbf7-463e-8698-a200e1cbbacb"

def push_gateway():
    counter = random.randint(100, 200)
    job_name = "pushgateway_" + str(req_id)
    Total_sleep_time = 0
    metric_name = "counter_seconds"
    labels = ["endpoint", "counter"]
    counter_seconds_metric = Gauge(metric_name, "Counter of seconds",labelnames=labels, registry=registry)
    for i in range (counter):
        Total_sleep_time += 1
        time.sleep(1)

    counter_seconds_metric.labels(endpoint=req_id , counter=counter).inc(Total_sleep_time)

    push_to_gateway(push_gateway_url, job=job_name, registry=registry)
    print("Metrics pushed to Prometheus Push Gateway")

    flock_data = {"text": f"Req_id-{req_id}, Counter-{counter}"}
    flock_resp = requests.post(flock_url, json=flock_data)
    print(flock_resp)

    return 0


if __name__ == '__main__':
    push_gateway()
